package in.co.rshop.ems.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import in.co.rshop.ems.entities.Faculty;
import in.co.rshop.ems.entities.Student;
import in.co.rshop.ems.entities.Subjects;
import in.co.rshop.ems.repo.FacultyRepo;
import in.co.rshop.ems.repo.StudentRepo;
import in.co.rshop.ems.repo.SubjectsRepo;

@RestController
public class EmsController {

	@Autowired
	private FacultyRepo facultyrepo;

	@GetMapping("/faculty")
	public List<Faculty> getFaculty() {
		return facultyrepo.findAll();
	}

	@Autowired
	private SubjectsRepo subjectsrepo;

	@GetMapping("/subjects")
	public List<Subjects> getSubjects() {
		return subjectsrepo.findAll();
	}
	@Autowired
	private StudentRepo studentrepo;
	
	@GetMapping("/students")
	public List<Student> getStudent(){
		return studentrepo.findAll();
	}

}
